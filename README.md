# Tema "fernandes" para o oh-my-zsh

- Personalização de tema para ser usado no oh-my-zsh.

## Passo 1
```
Para usá-lo, é necessário fazer a copia do arquivo "fernandes.zsh-theme" na seguinte pasta:

~$ .oh-my-zsh/themes/
```
## Passo 2
```
Depois é necessário editar o arquivo ~/.zshrc e alterar o valor da variável ZSH_THEME para:
ZSH_THEME="fernandes"
```
